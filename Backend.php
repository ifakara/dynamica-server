<?php

/**
 * Created by PhpStorm.
 * User: Wickerman
 * Date: 5/2/2017
 * Time: 11:27 AM
 */
class Backend
{

    private static $instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Backend();
        }

        return self::$instance;
    }

    public function getRoles()
    {
        $con = self::getConnection();

        $sql = $con->prepare("SELECT id,usergroup FROM access_groups");

        if ($sql->execute()) {
            $sql->store_result();
            $sql->bind_result($id, $userGroup);

            $results = array();
            while ($sql->fetch()) {
                array_push($results, array(
                    'id' => $id,
                    'group' => $userGroup
                ));
            }

            return $results;

        } else {

            return array();
        }
    }

    private function getConnection()
    {
        $host = 'localhost';
        $user = 'root';
        $pass = '';
        $dbname = 'dynamica';

        /*$host = 'localhost';
        $user = 'wickigcb_backend';
        $pass = 'z=%c=q(;Wt!i]BgneD';
        $dbname = 'wickigcb_dynamica';*/

        $con = mysqli_connect($host, $user, $pass, $dbname) or die(json_encode(array(
            'Error' => 'Failed to connect to database'
        )));

        return $con;
    }

    public function saveForm($name, $desc, $schema, $group)
    {

        $con = $this->getConnection();

        $con->autocommit(false);

        $sql = $con->prepare("INSERT INTO dyna_forms(form_name,description,json_schema,user_group) VALUES (?,?,?,?)");
        $sql->bind_param('sssi', $name, $desc, $schema, $group);

        if ($sql->execute()) {
            $con->commit();
        } else {
            echo $sql->error;
            $con->rollback();
        }
    }

    public function updateForm($id, $name, $desc, $schema, $group)
    {

        $con = $this->getConnection();

        $con->autocommit(false);

        $sql = $con->prepare("UPDATE dyna_forms SET form_name = ?, description = ?, json_schema = ?, user_group = ?, revision = revision + 1 WHERE id = ?");
        $sql->bind_param('sssii', $name, $desc, $schema, $group, $id);

        if ($sql->execute()) {
            $con->commit();
        } else {
            echo $sql->error;
            $con->rollback();
        }
    }

    public function getFormById($id)
    {
        $con = $this->getConnection();

        $sql = $con->prepare("SELECT json_schema FROM dyna_forms WHERE id = ?");
        $sql->bind_param('i', $id);

        if ($sql->execute()) {
            $sql->store_result();
            $sql->bind_result($schema);

            $sql->fetch();

            return $schema;

        } else {

            return '';
        }
    }

    public function getForms()
    {

        $con = $this->getConnection();

        $sql = $con->prepare("SELECT id,form_name,description,user_group,json_schema,revision,modified FROM dyna_forms ");

        if ($sql->execute()) {
            $sql->store_result();
            $sql->bind_result($id, $name, $desc, $group, $schema, $rev, $modified);

            $results = array();
            while ($sql->fetch()) {
                array_push($results, array(
                    'formId' => $id,
                    'formName' => $name,
                    'formDesc' => $desc,
                    'accessGroup' => $group,
                    'jsonSchema' => $schema,
                    'formRev' => $rev,
                    'modified' => $modified
                ));
            }

            return $results;

        } else {

            return array();
        }
    }

}